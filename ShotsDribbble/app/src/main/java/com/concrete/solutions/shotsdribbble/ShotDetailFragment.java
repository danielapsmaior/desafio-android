package com.concrete.solutions.shotsdribbble;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.concrete.solutions.shotsdribbble.models.Shot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a single Shot detail screen.
 * This fragment is either contained in a {@link ShotListActivity}
 * in two-pane mode (on tablets) or a {@link ShotDetailActivity}
 * on handsets.
 */
public class ShotDetailFragment extends Fragment {

    public static final String SHOT = "shot";
    private Shot shot;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ShotDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(SHOT)) {
            shot = (Shot)getArguments().getSerializable(SHOT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_shot_detail, container, false);

        if (shot != null) {
            ((TextView) rootView.findViewById(R.id.title)).setText(shot.getTitle());
            ((TextView) rootView.findViewById(R.id.viewsCount)).setText(Integer.toString(shot.getViewsCount()));


            Picasso.with(getActivity())
                    .load(shot.getImage400Url())
                    .placeholder(R.drawable.white)
                    .into((ImageView) rootView.findViewById(R.id.image400Url));

            ((TextView) rootView.findViewById(R.id.name)).setText(shot.getPlayer().getName());
            ((TextView) rootView.findViewById(R.id.description)).setText(shot.getDescription());

            Picasso.with(getActivity())
                    .load(shot.getPlayer().getAvatarUrl())
                    .placeholder(R.drawable.white)
                    .into((ImageView) rootView.findViewById(R.id.avatarUrl));
        }

        return rootView;
    }
}
