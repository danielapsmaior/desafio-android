package com.concrete.solutions.shotsdribbble.adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.concrete.solutions.shotsdribbble.R;
import com.concrete.solutions.shotsdribbble.models.Shot;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ShotListAdapter extends ArrayAdapter<Shot> {
    private LayoutInflater mInflater;

    public ShotListAdapter(Context context,List<Shot> objects) {
        super(context,  R.layout.shot_list_item, objects);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        Holder holder;

        if (view == null) {
            // View doesn't exist so create it and create the holder
            view = mInflater.inflate(R.layout.shot_list_item, parent, false);

            holder = new Holder();
            holder.image400Url = (ImageView) view.findViewById(R.id.image400Url);
            holder.title = (TextView) view.findViewById(R.id.title);
            holder.viewsCount = (TextView) view.findViewById(R.id.viewsCount);

            view.setTag(holder);
        } else {
            // Just get our existing holder
            holder = (Holder) view.getTag();
        }

        // Populate via the holder for speed
        Shot shot = getItem(position);

        // Populate the item contents
        holder.title.setText(shot.getTitle());
        holder.viewsCount.setText(Integer.toString(shot.getViewsCount()));

        // Load the screen cap image on a background thread
        Picasso.with(getContext())
                .load(shot.getImage400Url())
                .placeholder(R.drawable.white)
                .into(holder.image400Url);

        return view;
    }

    private static final class Holder {
        public ImageView image400Url;

        public TextView title;

        public TextView viewsCount;

    }
}
