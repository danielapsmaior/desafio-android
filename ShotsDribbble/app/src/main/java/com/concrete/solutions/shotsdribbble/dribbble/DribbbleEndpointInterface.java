package com.concrete.solutions.shotsdribbble.dribbble;

import com.concrete.solutions.shotsdribbble.models.Page;
import com.concrete.solutions.shotsdribbble.models.Shot;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface DribbbleEndpointInterface {
    // Request method and URL specified in the annotation
    // Callback for the parsed response is the last parameter

    @GET("/shots/{id}")
    void getShot(@Path("id") int id, Callback<Shot> cb);

    @GET("/shots/popular")
    void getPage(@Query("page") int page, @Query("per_page") int perPage, Callback<Page> cb);
}