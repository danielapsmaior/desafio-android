package com.concrete.solutions.shotsdribbble.dribbble;

import retrofit.RestAdapter;


public class DribbbleService {
    private static DribbbleEndpointInterface dribbleService;

    public static final String BASE_URL = "http://api.dribbble.com";


    public static DribbbleEndpointInterface getDribbleService() {
        if (dribbleService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(BASE_URL)
                    .build();

            dribbleService = restAdapter.create(DribbbleEndpointInterface.class);
        }

        return dribbleService;
    }
}