package com.concrete.solutions.shotsdribbble.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@Generated("org.jsonschema2pojo")
public class Page {

    @SerializedName("per_page")
    private int perPage;

    private String page;
    private int pages;
    private int total;
    private List<Shot> shots = new ArrayList<Shot>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The page
     */
    public String getPage() {
        return page;
    }

    /**
     *
     * @param page
     * The page
     */
    public void setPage(String page) {
        this.page = page;
    }

    public Page withPage(String page) {
        this.page = page;
        return this;
    }

    /**
     *
     * @return
     * The perPage
     */
    public int getPerPage() {
        return perPage;
    }

    /**
     *
     * @param perPage
     * The per_page
     */
    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public Page withPerPage(int perPage) {
        this.perPage = perPage;
        return this;
    }

    /**
     *
     * @return
     * The pages
     */
    public int getPages() {
        return pages;
    }

    /**
     *
     * @param pages
     * The pages
     */
    public void setPages(int pages) {
        this.pages = pages;
    }

    public Page withPages(int pages) {
        this.pages = pages;
        return this;
    }

    /**
     *
     * @return
     * The total
     */
    public int getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setTotal(int total) {
        this.total = total;
    }

    public Page withTotal(int total) {
        this.total = total;
        return this;
    }

    /**
     *
     * @return
     * The shots
     */
    public List<Shot> getShots() {
        return shots;
    }

    /**
     *
     * @param shots
     * The shots
     */
    public void setShots(List<Shot> shots) {
        this.shots = shots;
    }

    public Page withShots(List<Shot> shots) {
        this.shots = shots;
        return this;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Page withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}