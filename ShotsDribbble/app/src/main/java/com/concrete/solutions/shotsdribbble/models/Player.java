package com.concrete.solutions.shotsdribbble.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

//@Generated("org.jsonschema2pojo")
public class Player implements Serializable{

    private int id;
    private String name;
    private String username;
    private String location;
    private String url;

    @SerializedName("followers_count")
    private int followersCount;

    @SerializedName("draftees_count")
    private int drafteesCount;

    @SerializedName("likes_count")
    private int likesCount;

    @SerializedName("likes_received_count")
    private int likesReceivedCount;

    @SerializedName("comments_count")
    private int commentsCount;

    @SerializedName("comments_received_count")
    private int commentsReceivedCount;

    @SerializedName("rebounds_count")
    private int reboundsCount;

    @SerializedName("rebound_received_count")
    private int reboundsReceivedCount;

    @SerializedName("avatar_url")
    private String avatarUrl;

    @SerializedName("twitter_screen_name")
    private String twitterScreenName;

    @SerializedName("website_url")
    private String websiteUrl;

    @SerializedName("drafted_by_player_id")
    private int draftedByPlayerId;

    @SerializedName("shots_count")
    private int shotsCount;

    @SerializedName("following_count")
    private int followingCount;

    @SerializedName("created_at")
    private String createdAt;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(int id) {
        this.id = id;
    }

    public Player withId(int id) {
        this.id = id;
        return this;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public Player withName(String name) {
        this.name = name;
        return this;
    }

    /**
     *
     * @return
     * The location
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    public Player withLocation(String location) {
        this.location = location;
        return this;
    }

    /**
     *
     * @return
     * The followersCount
     */
    public int getFollowersCount() {
        return followersCount;
    }

    /**
     *
     * @param followersCount
     * The followers_count
     */
    public void setFollowersCount(int followersCount) {
        this.followersCount = followersCount;
    }

    public Player withFollowersCount(int followersCount) {
        this.followersCount = followersCount;
        return this;
    }

    /**
     *
     * @return
     * The drafteesCount
     */
    public int getDrafteesCount() {
        return drafteesCount;
    }

    /**
     *
     * @param drafteesCount
     * The draftees_count
     */
    public void setDrafteesCount(int drafteesCount) {
        this.drafteesCount = drafteesCount;
    }

    public Player withDrafteesCount(int drafteesCount) {
        this.drafteesCount = drafteesCount;
        return this;
    }

    /**
     *
     * @return
     * The likesCount
     */
    public int getLikesCount() {
        return likesCount;
    }

    /**
     *
     * @param likesCount
     * The likes_count
     */
    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public Player withLikesCount(int likesCount) {
        this.likesCount = likesCount;
        return this;
    }

    /**
     *
     * @return
     * The likesReceivedCount
     */
    public int getLikesReceivedCount() {
        return likesReceivedCount;
    }

    /**
     *
     * @param likesReceivedCount
     * The likes_received_count
     */
    public void setLikesReceivedCount(int likesReceivedCount) {
        this.likesReceivedCount = likesReceivedCount;
    }

    public Player withLikesReceivedCount(int likesReceivedCount) {
        this.likesReceivedCount = likesReceivedCount;
        return this;
    }

    /**
     *
     * @return
     * The commentsCount
     */
    public int getCommentsCount() {
        return commentsCount;
    }

    /**
     *
     * @param commentsCount
     * The comments_count
     */
    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public Player withCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
        return this;
    }

    /**
     *
     * @return
     * The commentsReceivedCount
     */
    public int getCommentsReceivedCount() {
        return commentsReceivedCount;
    }

    /**
     *
     * @param commentsReceivedCount
     * The comments_received_count
     */
    public void setCommentsReceivedCount(int commentsReceivedCount) {
        this.commentsReceivedCount = commentsReceivedCount;
    }

    public Player withCommentsReceivedCount(int commentsReceivedCount) {
        this.commentsReceivedCount = commentsReceivedCount;
        return this;
    }

    /**
     *
     * @return
     * The reboundsCount
     */
    public int getReboundsCount() {
        return reboundsCount;
    }

    /**
     *
     * @param reboundsCount
     * The rebounds_count
     */
    public void setReboundsCount(int reboundsCount) {
        this.reboundsCount = reboundsCount;
    }

    public Player withReboundsCount(int reboundsCount) {
        this.reboundsCount = reboundsCount;
        return this;
    }

    /**
     *
     * @return
     * The reboundsReceivedCount
     */
    public int getReboundsReceivedCount() {
        return reboundsReceivedCount;
    }

    /**
     *
     * @param reboundsReceivedCount
     * The rebounds_received_count
     */
    public void setReboundsReceivedCount(int reboundsReceivedCount) {
        this.reboundsReceivedCount = reboundsReceivedCount;
    }

    public Player withReboundsReceivedCount(int reboundsReceivedCount) {
        this.reboundsReceivedCount = reboundsReceivedCount;
        return this;
    }

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    public Player withUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     *
     * @return
     * The avatarUrl
     */
    public String getAvatarUrl() {
        return avatarUrl;
    }

    /**
     *
     * @param avatarUrl
     * The avatar_url
     */
    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Player withAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
        return this;
    }

    /**
     *
     * @return
     * The username
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     * The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    public Player withUsername(String username) {
        this.username = username;
        return this;
    }

    /**
     *
     * @return
     * The twitterScreenName
     */
    public String getTwitterScreenName() {
        return twitterScreenName;
    }

    /**
     *
     * @param twitterScreenName
     * The twitter_screen_name
     */
    public void setTwitterScreenName(String twitterScreenName) {
        this.twitterScreenName = twitterScreenName;
    }

    public Player withTwitterScreenName(String twitterScreenName) {
        this.twitterScreenName = twitterScreenName;
        return this;
    }

    /**
     *
     * @return
     * The websiteUrl
     */
    public String getWebsiteUrl() {
        return websiteUrl;
    }

    /**
     *
     * @param websiteUrl
     * The website_url
     */
    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public Player withWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
        return this;
    }

    /**
     *
     * @return
     * The draftedByPlayerId
     */
    public int getDraftedByPlayerId() {
        return draftedByPlayerId;
    }

    /**
     *
     * @param draftedByPlayerId
     * The drafted_by_player_id
     */
    public void setDraftedByPlayerId(int draftedByPlayerId) {
        this.draftedByPlayerId = draftedByPlayerId;
    }

    public Player withDraftedByPlayerId(int draftedByPlayerId) {
        this.draftedByPlayerId = draftedByPlayerId;
        return this;
    }

    /**
     *
     * @return
     * The shotsCount
     */
    public int getShotsCount() {
        return shotsCount;
    }

    /**
     *
     * @param shotsCount
     * The shots_count
     */
    public void setShotsCount(int shotsCount) {
        this.shotsCount = shotsCount;
    }

    public Player withShotsCount(int shotsCount) {
        this.shotsCount = shotsCount;
        return this;
    }

    /**
     *
     * @return
     * The followingCount
     */
    public int getFollowingCount() {
        return followingCount;
    }

    /**
     *
     * @param followingCount
     * The following_count
     */
    public void setFollowingCount(int followingCount) {
        this.followingCount = followingCount;
    }

    public Player withFollowingCount(int followingCount) {
        this.followingCount = followingCount;
        return this;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Player withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Player withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}