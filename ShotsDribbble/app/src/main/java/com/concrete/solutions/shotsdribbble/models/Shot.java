package com.concrete.solutions.shotsdribbble.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

//@Generated("org.jsonschema2pojo")
public class Shot implements Serializable{

    private int id;
    private String title;
    private String description;
    private int height;
    private int width;
    private String url;
    private Player player;

    @SerializedName("likes_count")
    private int likesCount;

    @SerializedName("comments_count")
    private int commentsCount;

    @SerializedName("rebounds_count")
    private int reboundsCount;

    @SerializedName("short_url")
    private String shortUrl;

    @SerializedName("views_count")
    private int viewsCount;

    @SerializedName("rebound_source_id")
    private Object reboundSourceId;

    @SerializedName("image_url")
    private String imageUrl;

    @SerializedName("image_teaser_url")
    private String imageTeaserUrl;

    @SerializedName("image_400_url")
    private String image400Url;

    @SerializedName("created_at")
    private String createdAt;

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @Override
    public String toString() {
        return title;
    }

    /**
     *
     * @return
     * The id
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(int id) {
        this.id = id;
    }

    public Shot withId(int id) {
        this.id = id;
        return this;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public Shot withTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public Shot withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     *
     * @return
     * The height
     */
    public int getHeight() {
        return height;
    }

    /**
     *
     * @param height
     * The height
     */
    public void setHeight(int height) {
        this.height = height;
    }

    public Shot withHeight(int height) {
        this.height = height;
        return this;
    }

    /**
     *
     * @return
     * The width
     */
    public int getWidth() {
        return width;
    }

    /**
     *
     * @param width
     * The width
     */
    public void setWidth(int width) {
        this.width = width;
    }

    public Shot withWidth(int width) {
        this.width = width;
        return this;
    }

    /**
     *
     * @return
     * The likesCount
     */
    public int getLikesCount() {
        return likesCount;
    }

    /**
     *
     * @param likesCount
     * The likes_count
     */
    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public Shot withLikesCount(int likesCount) {
        this.likesCount = likesCount;
        return this;
    }

    /**
     *
     * @return
     * The commentsCount
     */
    public int getCommentsCount() {
        return commentsCount;
    }

    /**
     *
     * @param commentsCount
     * The comments_count
     */
    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public Shot withCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
        return this;
    }

    /**
     *
     * @return
     * The reboundsCount
     */
    public int getReboundsCount() {
        return reboundsCount;
    }

    /**
     *
     * @param reboundsCount
     * The rebounds_count
     */
    public void setReboundsCount(int reboundsCount) {
        this.reboundsCount = reboundsCount;
    }

    public Shot withReboundsCount(int reboundsCount) {
        this.reboundsCount = reboundsCount;
        return this;
    }

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    public Shot withUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     *
     * @return
     * The shortUrl
     */
    public String getShortUrl() {
        return shortUrl;
    }

    /**
     *
     * @param shortUrl
     * The short_url
     */
    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public Shot withShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
        return this;
    }

    /**
     *
     * @return
     * The viewsCount
     */
    public int getViewsCount() {
        return viewsCount;
    }

    /**
     *
     * @param viewsCount
     * The views_count
     */
    public void setViewsCount(int viewsCount) {
        this.viewsCount = viewsCount;
    }

    public Shot withViewsCount(int viewsCount) {
        this.viewsCount = viewsCount;
        return this;
    }

    /**
     *
     * @return
     * The reboundSourceId
     */
    public Object getReboundSourceId() {
        return reboundSourceId;
    }

    /**
     *
     * @param reboundSourceId
     * The rebound_source_id
     */
    public void setReboundSourceId(Object reboundSourceId) {
        this.reboundSourceId = reboundSourceId;
    }

    public Shot withReboundSourceId(Object reboundSourceId) {
        this.reboundSourceId = reboundSourceId;
        return this;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Shot withImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    /**
     *
     * @return
     * The imageTeaserUrl
     */
    public String getImageTeaserUrl() {
        return imageTeaserUrl;
    }

    /**
     *
     * @param imageTeaserUrl
     * The image_teaser_url
     */
    public void setImageTeaserUrl(String imageTeaserUrl) {
        this.imageTeaserUrl = imageTeaserUrl;
    }

    public Shot withImageTeaserUrl(String imageTeaserUrl) {
        this.imageTeaserUrl = imageTeaserUrl;
        return this;
    }

    /**
     *
     * @return
     * The image400Url
     */

    public String getImage400Url() {
        return image400Url;
    }

    /**
     *
     * @param image400Url
     * The image_400_url
     */
    public void setImage400Url(String image400Url) {
        this.image400Url = image400Url;
    }

    public Shot withImage400Url(String image400Url) {
        this.image400Url = image400Url;
        return this;
    }

    /**
     *
     * @return
     * The player
     */
    public Player getPlayer() {
        return player;
    }

    /**
     *
     * @param player
     * The player
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    public Shot withPlayer(Player player) {
        this.player = player;
        return this;
    }

    /**
     *
     * @return
     * The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     * The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Shot withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Shot withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}